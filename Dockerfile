ARG ALPINE_VERSION=3.12
ARG DOCKER_VERSION=20.10.16
ARG DOCKER_COMPOSE_VERSION=1.27.4

FROM alpine:${ALPINE_VERSION} AS base

FROM base AS build-base
RUN apk add --no-cache curl

FROM docker/compose:alpine-${DOCKER_COMPOSE_VERSION} as docker-compose

FROM build-base as kubectl
ARG KUBECTL_VERSION=v1.19.2
RUN curl -fLSs https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl && \
    chmod +x /usr/local/bin/kubectl

FROM build-base as helm
ARG HELM_VERSION=v3.3.4
RUN curl -fLSs https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -o helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -zxvf helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm

FROM build-base as yc
# Yandex CLI get current version `curl https://storage.yandexcloud.net/yandexcloud-yc/release/stable`
ARG YC_VERSION=0.107.0
RUN curl -fLSs https://storage.yandexcloud.net/yandexcloud-yc/release/${YC_VERSION}/linux/amd64/yc -o /usr/local/bin/yc && \
    chmod a+x /usr/local/bin/yc

FROM build-base as hadolint
ARG HADOLINT_VERSION=v1.18.0
RUN curl -fLSs https://github.com/hadolint/hadolint/releases/download/${HADOLINT_VERSION}/hadolint-Linux-x86_64 -o /usr/local/bin/hadolint && \
    chmod +x /usr/local/bin/hadolint 


FROM docker:${DOCKER_VERSION}   

RUN apk add --no-cache openssl curl tar gzip bash jq

COPY --from=docker-compose /usr/local/bin/docker-compose  /usr/local/bin/docker-compose 
COPY --from=yc /usr/local/bin/yc /usr/local/bin/yc
COPY --from=kubectl /usr/local/bin/kubectl /usr/local/bin/kubectl
COPY --from=helm /usr/local/bin/helm /usr/local/bin/helm
COPY --from=hadolint /usr/local/bin/hadolint /usr/local/bin/hadolint

COPY src/ /build/
COPY charts/ /charts/
RUN ln -s /build/bin/* /usr/local/bin/